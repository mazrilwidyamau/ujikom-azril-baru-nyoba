<?php
defined('BASEPATH') or exit('No direct script access allowed');

class siswa extends CI_Controller
{
    public function index()
    {
        $data['title'] = "DASHBOARD";
        $data['siswa'] = $this->db->get_where('siswa', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->view('siswa/index', $data);
    }
}
